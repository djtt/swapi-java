package swapi.api;

import swapi.api.ApiUtils;

class FilmModel {
	private String title;
	private String url;
	private String id;
	public void setTitle(String title) {
		this.title = title;
	}
	public String getTitle(){
		return title;
	}
	public void setUrl(String url) {
		String subUrl = url.replace("https://swapi.co/api/films/", "");
		this.id = subUrl.replace("/", "");
		this.url = ApiUtils.replaceUrl(url);
	}
	public String getUrl(){
		return url;
	}
	public String getId() {
		return id;
	}
}

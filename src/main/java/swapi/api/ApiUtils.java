package swapi.api;

import org.springframework.beans.factory.annotation.Autowired;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

class ApiUtils {
	public static String target = "swapi.co";
	public static String replacement = "test.djtt.nl";
	
	public static String replaceUrl(String toEditUrl) {
		return toEditUrl.replace(target, replacement);
	}
	
	public static String[] replaceUrl(String[] toEditUrl) {
		String[] result = new String[toEditUrl.length];
		for (int i = 0; i < toEditUrl.length; i++) {
			result[i] = toEditUrl[i].replace(target, replacement);
		}
		return result;
	}

	public static HttpEntity<String> headerEntity() {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
		return new HttpEntity<String>("parameters", headers);
	}
	
	public static String getUrl(String url) {
		HttpEntity<String> head = headerEntity();
		String result = "";
		RestTemplate restTemplate = new RestTemplate();
		switch (url.substring(0, 24)) {
			case "https://swapi.co/api/pla":
			case "https://swapi.co/api/sta":
			case "https://swapi.co/api/veh":
				ResponseEntity<NameModel> nameResponse = restTemplate.exchange(url, HttpMethod.GET, head, NameModel.class);
				result = nameResponse.toString();
				break;
			case "https://swapi.co/api/fil":
				ResponseEntity<FilmModel> filmResponse = restTemplate.exchange(url, HttpMethod.GET, head, FilmModel.class);
				result = filmResponse.toString();
				break;
		}
		int maxLength = result.length();
		String trimString = result.substring(5, maxLength - 6);
		maxLength = trimString.indexOf(",");
		return trimString.substring(0, maxLength);
	}
	public static String[] getUrl(String[] url) {
		String[] result = new String[url.length];
		for (int i = 0; i < url.length; i++) {
			result[i] = getUrl(url[i]);
		}
		return result;
	}
}
package swapi.api;

import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import swapi.api.ApiUtils;
 
@RestController
@RequestMapping("/films")
public class FilmController {
	@Autowired
	RestTemplate restTemplate;
	
	public String searchUrl = "https://swapi.co/api/films/";
	public String url = searchUrl + "/";

	@GetMapping(path={"/", ""}, produces="Application/json")
	public MultipleFilmInfo getFilms() {
		ResponseEntity<MultipleFilmInfo> response = restTemplate.exchange(url, HttpMethod.GET, ApiUtils.headerEntity(), MultipleFilmInfo.class);
		MultipleFilmInfo filmList = response.getBody();
		return filmList;
	}
	
	@GetMapping(path={"/{id}", "/{id}/"}, produces="Application/json")
	public FilmModel getFilm(@PathVariable String id) {
		ResponseEntity<FilmModel> response = restTemplate.exchange(url + id, HttpMethod.GET, ApiUtils.headerEntity(), FilmModel.class);
		FilmModel film = response.getBody();
		return film;
	}
}

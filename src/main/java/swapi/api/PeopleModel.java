package swapi.api;

import swapi.api.ApiUtils;

class PeopleModel {
	private String birth_year;
	private String eye_color;
	private String[] films;
	private String gender;
	private String hair_color;
	private String height;
	private String homeworld;
	private String homeworldName;
	private String mass;
	private String name;
	private String skin_color;
	private String created;
	private String edited;
//	private String[] species;
//	private String[] starships;
	private String url;
	private String id;
//	private String[] vehicles;
	public void setFilms(String[] films) {
		this.films = ApiUtils.replaceUrl(films);
	}
	public String[] getFilms() {
		return films;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getGender(){
		return gender;
	}
	public void setHair_color(String hair_color) {
		this.hair_color = hair_color;
	}
	public void setHomeworld(String homeworld) {
		this.homeworld = ApiUtils.replaceUrl(homeworld);
	}
	public String getHomeworld(){
		return homeworld;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getName(){
		return name;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	public String getCreated(){
		return created;
	}
	public void setEdited(String edited) {
		this.edited = edited;
	}
	public String getEdited(){
		return edited;
	}
//	public void setStarships(String[] starships) {
//		this.starships = ApiUtils.getUrl(starships);
//	}
//	public String[] getStarships() {
//		return starships;
//	}
	public void setUrl(String url) {
		String subUrl = url.replace("https://swapi.co/api/people/", "");
		this.id = subUrl.replace("/", "");
		this.url = ApiUtils.replaceUrl(url);
	}
	public String getUrl(){
		return url;
	}
	public String getId() {
		return id;
	}
//	public void setVehicles(String[] vehicles) {
//		this.vehicles = ApiUtils.getUrl(vehicles);
//	}
//	public String[] getVehicles() {
//		return vehicles;
//	}
}

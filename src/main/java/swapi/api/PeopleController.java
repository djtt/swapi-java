package swapi.api;

import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import swapi.api.ApiUtils;
 
@RestController
@RequestMapping("/people")
public class PeopleController {
	@Autowired
	RestTemplate restTemplate;
	
	public String searchUrl = "https://swapi.co/api/people/";
	public String url;

	@GetMapping(path={"/", ""}, produces="Application/json")
	public MultiplePeopleInfo getMultiplePeople(
			@RequestParam(value="search", required=false) String search, 
			@RequestParam(value="page", required=false) String page) {
		if (search != null) {
			url = searchUrl + "?search=" + search;
		} else if (page != null) {
			url = searchUrl + "?page=" + page;
		} else {
			url = searchUrl;
		}
		ResponseEntity<MultiplePeopleInfo> response = restTemplate.exchange(url, HttpMethod.GET, ApiUtils.headerEntity(), MultiplePeopleInfo.class);
		MultiplePeopleInfo peopleList = response.getBody();
		return peopleList;
	}
	
	@GetMapping(path={"/{id}", "/{id}/"}, produces="Application/json")
	public PeopleModel getPeople(@PathVariable String id) {
		ResponseEntity<PeopleModel> response = restTemplate.exchange(url + id, HttpMethod.GET, ApiUtils.headerEntity(), PeopleModel.class);
		PeopleModel people = response.getBody();
		return people;
	}
	
}

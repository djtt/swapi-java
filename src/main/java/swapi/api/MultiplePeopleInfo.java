package swapi.api;

import swapi.api.ApiUtils;

public class MultiplePeopleInfo {
	private String count;
	private String next;
	private String previous;
	private PeopleModel[] peopleModel;
	
	public void setCount(String count) {
		this.count = count;
	}
	public String getCount() {
		return count;
	}
	public void setNext(String next) {
		if (next != null) {
			this.next = ApiUtils.replaceUrl(next);
		} else {
			this.next = next;
		}
	}
	public String getNext() {
		return next;
	}
	public void setPrevious(String previous) {
		if (previous != null) {
			this.previous = ApiUtils.replaceUrl(previous);
		} else {
			this.previous = previous;
		}
	}
	public String getPrevious() {
		return previous;
	}
	
	public void setResults(PeopleModel[] peopleModel) {
		this.peopleModel = peopleModel;
	}
	public PeopleModel[] getResults() {
		return peopleModel;
	}
	
	
}

package swapi.api;

import swapi.api.ApiUtils;

class PeopleModelLight {
	private String name;
	private String url;
	private String id;
	public void setName(String name) {
		this.name = name;
	}
	public String getName(){
		return name;
	}
	public void setUrl(String url) {
		String subUrl = url.replace("https://swapi.co/api/people/", "");
		this.id = subUrl.replace("/", "");
		this.url = ApiUtils.replaceUrl(url);
	}
	public String getUrl(){
		return url;
	}
	public String getId() {
		return id;
	}
}
